import DomElement from "./dom-element"
import { v4 } from "uuid"

window.MediaContext = window.MediaContext||window.webkitMediaContext

export default class MediaPlayer extends DomElement {
  constructor(){
    super()
    if (window.MP){
      window.MP.destroy()
    }
    window.MP = this

    this.loaded = false
    this.playing = false
    this.currentMedia = {
      path: null,
      title: null
    }
    this.id = v4()
  }

  init() {
    const element = (this.mediaPath.includes('.mp4')) ? 'VIDEO' : 'AUDIO'
    this.dom = document.createElement(element)
    this.dom.id = `mp-${this.id}`
    this.dom.autoplay = true
    this.dom.controls = false
    this.dom.setAttribute('playsinline', true)
    this.style({
      width: '100%',
      position: 'absolute',
      top: 0,
      left: 0,
    })
    if (this.isIOS()) this.dom.autoplay = true
    this.appendTo()
  }

  loadFile(mediaPath, mediaTitle='Untitled') {
    this.currentMedia.path = mediaPath
    this.currentMedia.title = mediaTitle
    this.dom.onloadeddata = this.onFileLoad.bind(this)
    this.dom.onended = this.finish.bind(this)
    this.dom.src = mediaPath
  }

  finish(){
    this.dispatchEvent( "song-finished" )
  }

  onFileLoad(){
    this.loaded = true
    this.dispatchEvent( "song-loaded" )
  }

  play(){
    if (!this.loaded){
      throw 'Media cannot play because it is not loaded.'
    } else if (this.playing) {
      console.error('Media is already playing!')
      return
    }
    this.dispatchEvent('song-started');
    this.playing = true
    this.dom.play()
  }

  pause(){
    this.playing = false
    if (typeof this.dom.pause !== 'undefined')
      this.dom.pause() // pause event will dispatch
  }

  stop(){
    this.pause()
    this.setPosition(0)
  }

  getPosition(){
    return this.dom.currentTime
  }

  setPosition(pos){
    this.dom.currentTime = pos
  }

  destroy(){
    this.stop()
//    this.dom.parentNode.removeChild(this.dom)
//    delete this
//    delete window.MP
  }
}
