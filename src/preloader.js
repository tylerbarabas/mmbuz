import DomElement from './dom-element'

const supported_types = {
  image: [
    'png',
    'svg',
    'jpg',
    'gif',
  ],
  video: [
    'mp4',
  ],
  audio: [
    'mp3',
    'wav',
    'm4a',
  ],
}
export default class Preloader extends DomElement {
  constructor(assets=[]){
    super()
    this.assets = assets
    this.assetsLoading = []
    this.assetsFinished = []

    this.queueLimit = 5
    this.queue = []

    this.totalToLoad = 0
    this.stackChecker = null
  }
  load(assets){
    if (!Array.isArray(assets)){
      throw 'Cannot instantiate Preloader.  Assets argument must be an array.'
    }
  }
  preloadOneAsset(asset){
    if (
      typeof asset === null
      || typeof asset === 'undefined'
    ) throw 'No preload asset given'

    const ext = this.getExtension(asset)
    const type = this.getTypeFromExtension(ext) 
    if (type === null) throw `File extension not recognized for ${asset}.`
    let element = null
    switch(type){
      case 'image':
        element = document.createElement('IMG')
        element.addEventListener('load',this.assetLoaded.bind(this))
        element.src = asset
      break
      case 'video':
        element = document.createElement('VIDEO')
        element.onloadeddata = this.assetLoaded.bind(this)
        element.src = asset
      break
      case 'audio':
        element = document.createElement('AUDIO')
        element.onloadeddata = this.assetLoaded.bind(this)
        element.src = asset
      default:
      break
    }
    return asset
  }

  assetLoaded(e){
    this.assetsFinished.push(e.target.src)
    const index = this.assetsLoading.indexOf(e.target.src)
    this.assetsLoading.splice(index,1)
    const pc = this.getPercentComplete()
    if (pc === 100) this.dispatchEvent('preloader-finished')
    this.dispatchEvent('preloader-asset-loaded', {
      percentComplete: this.getPercentComplete(),
    })
    e.target.removeEventListener('loadeddata', this.assetLoaded.bind(this))
  }

  getPercentComplete(){
    const numAssets = this.assets.length
    const percentRemaining = (this.getNumberOfAssetsRemaining() / numAssets) * 100
    return 100 - Math.round(percentRemaining)
  }

  getNumberOfAssetsRemaining(){
    return this.assets.length - this.assetsFinished.length
  }

  getTypeFromExtension(ext){
    let type = null
    if (supported_types.image.indexOf(ext) !== -1) {
      type = 'image'
    } else if (supported_types.video.indexOf(ext) !== -1) {
      type = 'video'
    } else if (supported_types.audio.indexOf(ext) !== -1) {
      type = 'audio'
    }
    return type
  }

  getExtension(file){
    const arr = file.split('.')
    return arr[arr.length-1].toLowerCase()
  }

  preloadAllAssets(){
    if (this.isIOS()){
      this.assets = this.assets.filter(a=>{
        const type = this.getTypeFromExtension(this.getExtension(a))
        return type !== 'audio'&& type !== 'video'
      })
    }
    let index = 0
    this.stackChecker = setInterval(()=>{
      if (this.assetsLoading.length < 5) {
        if (this.assets[index]) {
          this.assetsLoading.push(this.preloadOneAsset(this.assets[index]))
          index++
        } else {
          clearInterval(this.stackChecker)
        }
      }
    },1)
  }
}
